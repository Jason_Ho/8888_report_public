module.exports = {
  /**
   * Application configuration section
   * http://pm2.keymetrics.io/docs/usage/application-declaration/
   */
  apps : [
    {
      name      : 'CRM',
      script    : 'bin/www',
      watch     : true,
      ignore_watch : ["tmp"],
      instances  : 2,
      exec_mode : "cluster"
    }
  ]
};
