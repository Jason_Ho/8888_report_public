const express = require('express');
const router = express.Router();
const request = require('request');
const msgHandler = require('./msghandler');

const multer = require('multer')
// 定義form parser儲存位置以及檔名格式
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, '/tmp/')
    },
    filename: function (req, file, cb) {
        cb(null, `${Date.now()}.${file.originalname.split('.').pop()}`)
    }
})
// 建立multer()物件
const FormParser = multer({
    preservePath: true,
    storage: storage
})

/* GET home page. */
router.get('/', function (req, res) {

    /*Worker list */
    let list = ['Workers', 'Undocumented Workers', 'Professionals, Temporary Visitors', 'Immigrants ']
    let worklist = JSON.stringify(list).replace(/"/g, '\'');

    /*取得Category清單 */
    //127.1.7.2測試站環境
    let searchapi = 'http://172.1.2.7:8080/ArcareEng/ProjectQueryService'
    let body = {
        strProjectId: '{C7E68572-BF70-0001-3C5A-148077023840}',
        strCompanyId: '{C7E68593-F1D0-0001-6B25-5DCA14601CA9}',
        strApiId: 'dept_list_OFW',
        oParamValue: {
            "": ''
        }
    }

    //正式站環境
    // let searchapi = 'http://localhost:8080/ArcareEng/ProjectQueryService'
    // let body = {
    //   strProjectId: '{C7EBA889-1330-0001-4CB5-1E70B38E18D1}',
    //   strCompanyId: '{C7EBA8A5-D790-0001-BFAB-26565D0F1AA8}',
    //   strApiId: 'dept_list_OFW',
    //   oParamValue: {
    //     "": ''
    //   }
    // }

    request.post({
        headers: {
            'Content-Type': 'text/plain;charset=UTF-8'
        },
        url: searchapi,
        form: JSON.stringify(body)
    }, function (error, response, body) {
        /*取出陣列唯一值 */
        let results = JSON.parse(body).record;
        let caterepeat = [];
        let json = JSON.stringify(JSON.parse(body).record).replace(/"/g, '\'');
        // console.log(results);//maincate清單

        results.forEach(function (element) {
            caterepeat.push(element.MCATEGORY)
        });
        let maincat = JSON.stringify(Array.from(new Set(caterepeat))).replace(/"/g, '\'');
        console.log('Get Category success!');
        res.render('report_ofw', {
            maincat: maincat,
            results: json,
            worklist: worklist
        });
    });
});

/* POST form page. */
router.post('/', FormParser.array('upimg'), function (req, res) {

    // let upimg;
    let upimgs = req.files
    let fields = req.body
    let n = new Date();
    let m = ("0" + (n.getMonth() + 1)).slice(-2);
    let msgdatetime = n.getFullYear() + "-" + m + "-" + ("0" + n.getDate()).slice(-2) + " " + ("0" + n.getHours()).slice(-2) + ":" + ("0" + n.getMinutes()).slice(-2) + ":" + ("0" + n.getSeconds()).slice(-2);


    let msgInfo = {
        msgBody: {
            pid: fields.pid,
            name: fields.name,
            mobile: fields.mobile,
            mcontent: fields.content,
            maincat: fields.maincat,
            subcat: fields.subcat,
            dept: fields.dept,
            manpower:fields.manpower,
            area:"OFW"
        },
        datetime: msgdatetime,
        imgs: []
    }

    if (upimgs.length != 0) {
        upimgs.forEach(upimg => {
            msgInfo.imgs.push({
                filename: upimg.filename,
                path: upimg.path
            })
        })
    }

    if (msgInfo.msgBody.name === undefined) {
        msgInfo.msgBody.name = 'Anonymous';
        msgInfo.msgBody.mobile = '09000000000';
    }

    let msg = new msgHandler.Message(msgInfo)
    msg.seeVar()
    msg.msghander()
    res.send(true)
});

module.exports = router;