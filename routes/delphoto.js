var express = require('express');
var router = express.Router();
const AWS = require('aws-sdk');
const s3 = new AWS.S3();
var request = require('request');


router.get('/', function (req, res, next) {

  res.render('delphoto', {
    title: 'Delete the S3 photo'
  });
});

router.post('/', function (req, res, next) {

  //delete S3 file
  console.log(req.body);
  let result = req.body.key;
  let filename = result.split("/");

  if (result != '') {
    let params = {
      Bucket: 'project-crm',
      Key: filename[4],
      //Key: result,
    };
    console.log(req.body.key);
    s3.deleteObject(params, function aaa(err, data) {
      if (err) {
        console.log(err);
        res.send(JSON.stringify({ "res": "Delete Photo error!" }))
      } else {
        console.log("Filename : " + params.Key);
        console.log('Delete Photo success!');
        res.send(JSON.stringify({ "res": "Delete Photo success!" }))
      }
    })

  } else {
    console.log("No Photo need delete")
    res.send(JSON.stringify({ "res": "No photo need delete!" }))
  }


})


module.exports = router;