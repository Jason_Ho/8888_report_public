/* eslint-disable */
import CryptoJS from 'crypto-js';
import sjcl from 'sjcl';

export function guid() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  // return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
  return s4().concat(s4(), '-', s4(), '-', s4(), '-', s4(), '-', s4(), s4(), s4());
}

function assertString(input) {
  if (typeof input !== 'string') {
    throw new TypeError('This library (validator.js) validates strings only');
  }
}

export function isUUID(str) {
  assertString(str);
  const pattern = /^[0-9A-F]{8}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{12}$/i;
  return pattern && pattern.test(str);
}

export function hash256(str) {
  /* eslint new-cap: ["error", { "properties": false }]*/
  return CryptoJS.enc.Base64.stringify(CryptoJS.SHA256(str));
}

export function calculatePwHash(pw) {
  return hash256(pw);
}

export function calculateAuth(pwHash, salt, nonce) {
  return hash256(pwHash + salt + nonce);
}

export function randomString() {
  return CryptoJS.enc.Hex.stringify(CryptoJS.lib.WordArray.random(20));
}

export function calculateSecret(uid, nonce, token) {
  return hash256(`nim:${uid}${nonce}${token}`);
}

export function idIsEmail(id) {
  if (id.indexOf('@') !== -1) {
    // console.log('email');
    return true;
  }
  // console.log('mobile');
  return false;
}

export function getIdentity(id) {
  if (id.indexOf('@') !== -1) {
    // console.log('email');
    return getEmailIdentity(id);
  }
  // console.log('mobile');
  return getPhoneIdentity(id);
}

export function getEmailIdentity(email) {
  return { type: 'email', id: email };
}

export function getPhoneIdentity(phone) {
  return { type: 'mobile', id: phone };
}

export function getNameIdentity(name) {
  return { type: 'name', id: name };
}

/** @fileOverview Javascript SHA-512 implementation.
 *
 * This implementation was written for CryptoJS by Jeff Mott and adapted for
 * SJCL by Stefan Thomas.
 *
 * CryptoJS (c) 2009–2012 by Jeff Mott. All rights reserved.
 * Released with New BSD License
 *
 * @author Emily Stark
 * @author Mike Hamburg
 * @author Dan Boneh
 * @author Jeff Mott
 * @author Stefan Thomas
 */

/**
 * Context for a SHA-512 operation in progress.
 * @constructor
 */
sjcl.hash.sha512 = function (hash) {
  if (!this._key[0]) {
    this._precompute();
  }
  if (hash) {
    this._h = hash._h.slice(0);
    this._buffer = hash._buffer.slice(0);
    this._length = hash._length;
  } else {
    this.reset();
  }
};

/**
 * Hash a string or an array of words.
 * @static
 * @param {bitArray|String} data the data to hash.
 * @return {bitArray} The hash value, an array of 16 big-endian words.
 */
sjcl.hash.sha512.hash = function (data) {
  return new sjcl.hash.sha512().update(data).finalize();
};

sjcl.hash.sha512.prototype = {
  /**
   * The hash's block size, in bits.
   * @constant
   */
  blockSize: 1024,

  /**
   * Reset the hash state.
   * @return this
   */
  reset() {
    this._h = this._init.slice(0);
    this._buffer = [];
    this._length = 0;
    return this;
  },

  /**
   * Input several words to the hash.
   * @param {bitArray|String} data the data to hash.
   * @return this
   */
  update(data) {
    if (typeof data === 'string') {
      data = sjcl.codec.utf8String.toBits(data);
    }
    let i,
      b = (this._buffer = sjcl.bitArray.concat(this._buffer, data)),
      ol = this._length,
      nl = (this._length = ol + sjcl.bitArray.bitLength(data));
    if (nl > 9007199254740991) {
      throw new sjcl.exception.invalid('Cannot hash more than 2^53 - 1 bits');
    }

    if (typeof Uint32Array !== 'undefined') {
      const c = new Uint32Array(b);
      let j = 0;
      for (i = 1024 + ol - ((1024 + ol) & 1023); i <= nl; i += 1024) {
        this._block(c.subarray(32 * j, 32 * (j + 1)));
        j += 1;
      }
      b.splice(0, 32 * j);
    } else {
      for (i = 1024 + ol - ((1024 + ol) & 1023); i <= nl; i += 1024) {
        this._block(b.splice(0, 32));
      }
    }
    return this;
  },

  /**
   * Complete hashing and output the hash value.
   * @return {bitArray} The hash value, an array of 16 big-endian words.
   */
  finalize() {
    let i,
      b = this._buffer,
      h = this._h;

    // Round out and push the buffer
    b = sjcl.bitArray.concat(b, [sjcl.bitArray.partial(1, 1)]);

    // Round out the buffer to a multiple of 32 words, less the 4 length words.
    for (i = b.length + 4; i & 31; i++) {
      b.push(0);
    }

    // append the length
    b.push(0);
    b.push(0);
    b.push(Math.floor(this._length / 0x100000000));
    b.push(this._length | 0);

    while (b.length) {
      this._block(b.splice(0, 32));
    }

    this.reset();
    return h;
  },

  /**
   * The SHA-512 initialization vector, to be precomputed.
   * @private
   */
  _init: [],

  /**
   * Least significant 24 bits of SHA512 initialization values.
   *
   * Javascript only has 53 bits of precision, so we compute the 40 most
   * significant bits and add the remaining 24 bits as constants.
   *
   * @private
   */
  _initr: [0xbcc908, 0xcaa73b, 0x94f82b, 0x1d36f1, 0xe682d1, 0x3e6c1f, 0x41bd6b, 0x7e2179],

  /**
   * The SHA-512 hash key, to be precomputed.
   * @private
   */
  _key: [],

  /**
   * Least significant 24 bits of SHA512 key values.
   * @private
   */
  _keyr: [
    0x28ae22,
    0xef65cd,
    0x4d3b2f,
    0x89dbbc,
    0x48b538,
    0x05d019,
    0x194f9b,
    0x6d8118,
    0x030242,
    0x706fbe,
    0xe4b28c,
    0xffb4e2,
    0x7b896f,
    0x1696b1,
    0xc71235,
    0x692694,
    0xf14ad2,
    0x4f25e3,
    0x8cd5b5,
    0xac9c65,
    0x2b0275,
    0xa6e483,
    0x41fbd4,
    0x1153b5,
    0x66dfab,
    0xb43210,
    0xfb213f,
    0xef0ee4,
    0xa88fc2,
    0x0aa725,
    0x03826f,
    0x0e6e70,
    0xd22ffc,
    0x26c926,
    0xc42aed,
    0x95b3df,
    0xaf63de,
    0x77b2a8,
    0xedaee6,
    0x82353b,
    0xf10364,
    0x423001,
    0xf89791,
    0x54be30,
    0xef5218,
    0x65a910,
    0x71202a,
    0xbbd1b8,
    0xd2d0c8,
    0x41ab53,
    0x8eeb99,
    0x9b48a8,
    0xc95a63,
    0x418acb,
    0x63e373,
    0xb2b8a3,
    0xefb2fc,
    0x172f60,
    0xf0ab72,
    0x6439ec,
    0x631e28,
    0x82bde9,
    0xc67915,
    0x72532b,
    0x26619c,
    0xc0c207,
    0xe0eb1e,
    0x6ed178,
    0x176fba,
    0xc898a6,
    0xf90dae,
    0x1c471b,
    0x047d84,
    0xc72493,
    0xc9bebc,
    0x100d4c,
    0x3e42b6,
    0x657e2a,
    0xd6faec,
    0x475817,
  ],

  /**
   * Function to precompute _init and _key.
   * @private
   */
  _precompute() {
    // XXX: This code is for precomputing the SHA256 constants, change for
    //      SHA512 and re-enable.
    let i = 0,
      prime = 2,
      factor,
      isPrime;

    function frac(x) {
      return ((x - Math.floor(x)) * 0x100000000) | 0;
    }
    function frac2(x) {
      return ((x - Math.floor(x)) * 0x10000000000) & 0xff;
    }

    for (; i < 80; prime++) {
      isPrime = true;
      for (factor = 2; factor * factor <= prime; factor++) {
        if (prime % factor === 0) {
          isPrime = false;
          break;
        }
      }
      if (isPrime) {
        if (i < 8) {
          this._init[i * 2] = frac(Math.pow(prime, 1 / 2));
          this._init[i * 2 + 1] = (frac2(Math.pow(prime, 1 / 2)) << 24) | this._initr[i];
        }
        this._key[i * 2] = frac(Math.pow(prime, 1 / 3));
        this._key[i * 2 + 1] = (frac2(Math.pow(prime, 1 / 3)) << 24) | this._keyr[i];
        i++;
      }
    }
  },

  /**
   * Perform one cycle of SHA-512.
   * @param {Uint32Array|bitArray} words one block of words.
   * @private
   */
  _block(words) {
    let i,
      wrh,
      wrl,
      h = this._h,
      k = this._key,
      h0h = h[0],
      h0l = h[1],
      h1h = h[2],
      h1l = h[3],
      h2h = h[4],
      h2l = h[5],
      h3h = h[6],
      h3l = h[7],
      h4h = h[8],
      h4l = h[9],
      h5h = h[10],
      h5l = h[11],
      h6h = h[12],
      h6l = h[13],
      h7h = h[14],
      h7l = h[15];
    let w;
    if (typeof Uint32Array !== 'undefined') {
      // When words is passed to _block, it has 32 elements. SHA512 _block
      // function extends words with new elements (at the end there are 160 elements).
      // The problem is that if we use Uint32Array instead of Array,
      // the length of Uint32Array cannot be changed. Thus, we replace words with a
      // normal Array here.
      w = Array(160); // do not use Uint32Array here as the instantiation is slower
      for (let j = 0; j < 32; j++) {
        w[j] = words[j];
      }
    } else {
      w = words;
    }

    // Working variables
    let ah = h0h,
      al = h0l,
      bh = h1h,
      bl = h1l,
      ch = h2h,
      cl = h2l,
      dh = h3h,
      dl = h3l,
      eh = h4h,
      el = h4l,
      fh = h5h,
      fl = h5l,
      gh = h6h,
      gl = h6l,
      hh = h7h,
      hl = h7l;

    for (i = 0; i < 80; i++) {
      // load up the input word for this round
      if (i < 16) {
        wrh = w[i * 2];
        wrl = w[i * 2 + 1];
      } else {
        // Gamma0
        const gamma0xh = w[(i - 15) * 2];
        const gamma0xl = w[(i - 15) * 2 + 1];
        const gamma0h =
          ((gamma0xl << 31) | (gamma0xh >>> 1)) ^ ((gamma0xl << 24) | (gamma0xh >>> 8)) ^ (gamma0xh >>> 7);
        const gamma0l =
          ((gamma0xh << 31) | (gamma0xl >>> 1)) ^
          ((gamma0xh << 24) | (gamma0xl >>> 8)) ^
          ((gamma0xh << 25) | (gamma0xl >>> 7));

        // Gamma1
        const gamma1xh = w[(i - 2) * 2];
        const gamma1xl = w[(i - 2) * 2 + 1];
        const gamma1h =
          ((gamma1xl << 13) | (gamma1xh >>> 19)) ^ ((gamma1xh << 3) | (gamma1xl >>> 29)) ^ (gamma1xh >>> 6);
        const gamma1l =
          ((gamma1xh << 13) | (gamma1xl >>> 19)) ^
          ((gamma1xl << 3) | (gamma1xh >>> 29)) ^
          ((gamma1xh << 26) | (gamma1xl >>> 6));

        // Shortcuts
        const wr7h = w[(i - 7) * 2];
        const wr7l = w[(i - 7) * 2 + 1];

        const wr16h = w[(i - 16) * 2];
        const wr16l = w[(i - 16) * 2 + 1];

        // W(round) = gamma0 + W(round - 7) + gamma1 + W(round - 16)
        wrl = gamma0l + wr7l;
        wrh = gamma0h + wr7h + (wrl >>> 0 < gamma0l >>> 0 ? 1 : 0);
        wrl += gamma1l;
        wrh += gamma1h + (wrl >>> 0 < gamma1l >>> 0 ? 1 : 0);
        wrl += wr16l;
        wrh += wr16h + (wrl >>> 0 < wr16l >>> 0 ? 1 : 0);
      }

      w[i * 2] = wrh |= 0;
      w[i * 2 + 1] = wrl |= 0;

      // Ch
      const chh = (eh & fh) ^ (~eh & gh);
      const chl = (el & fl) ^ (~el & gl);

      // Maj
      const majh = (ah & bh) ^ (ah & ch) ^ (bh & ch);
      const majl = (al & bl) ^ (al & cl) ^ (bl & cl);

      // Sigma0
      const sigma0h = ((al << 4) | (ah >>> 28)) ^ ((ah << 30) | (al >>> 2)) ^ ((ah << 25) | (al >>> 7));
      const sigma0l = ((ah << 4) | (al >>> 28)) ^ ((al << 30) | (ah >>> 2)) ^ ((al << 25) | (ah >>> 7));

      // Sigma1
      const sigma1h = ((el << 18) | (eh >>> 14)) ^ ((el << 14) | (eh >>> 18)) ^ ((eh << 23) | (el >>> 9));
      const sigma1l = ((eh << 18) | (el >>> 14)) ^ ((eh << 14) | (el >>> 18)) ^ ((el << 23) | (eh >>> 9));

      // K(round)
      const krh = k[i * 2];
      const krl = k[i * 2 + 1];

      // t1 = h + sigma1 + ch + K(round) + W(round)
      let t1l = hl + sigma1l;
      let t1h = hh + sigma1h + (t1l >>> 0 < hl >>> 0 ? 1 : 0);
      t1l += chl;
      t1h += chh + (t1l >>> 0 < chl >>> 0 ? 1 : 0);
      t1l += krl;
      t1h += krh + (t1l >>> 0 < krl >>> 0 ? 1 : 0);
      t1l = (t1l + wrl) | 0; // FF32..FF34 perf issue https://bugzilla.mozilla.org/show_bug.cgi?id=1054972
      t1h += wrh + (t1l >>> 0 < wrl >>> 0 ? 1 : 0);

      // t2 = sigma0 + maj
      const t2l = sigma0l + majl;
      const t2h = sigma0h + majh + (t2l >>> 0 < sigma0l >>> 0 ? 1 : 0);

      // Update working variables
      hh = gh;
      hl = gl;
      gh = fh;
      gl = fl;
      fh = eh;
      fl = el;
      el = (dl + t1l) | 0;
      eh = (dh + t1h + (el >>> 0 < dl >>> 0 ? 1 : 0)) | 0;
      dh = ch;
      dl = cl;
      ch = bh;
      cl = bl;
      bh = ah;
      bl = al;
      al = (t1l + t2l) | 0;
      ah = (t1h + t2h + (al >>> 0 < t1l >>> 0 ? 1 : 0)) | 0;
    }

    // Intermediate hash
    h0l = h[1] = (h0l + al) | 0;
    h[0] = (h0h + ah + (h0l >>> 0 < al >>> 0 ? 1 : 0)) | 0;
    h1l = h[3] = (h1l + bl) | 0;
    h[2] = (h1h + bh + (h1l >>> 0 < bl >>> 0 ? 1 : 0)) | 0;
    h2l = h[5] = (h2l + cl) | 0;
    h[4] = (h2h + ch + (h2l >>> 0 < cl >>> 0 ? 1 : 0)) | 0;
    h3l = h[7] = (h3l + dl) | 0;
    h[6] = (h3h + dh + (h3l >>> 0 < dl >>> 0 ? 1 : 0)) | 0;
    h4l = h[9] = (h4l + el) | 0;
    h[8] = (h4h + eh + (h4l >>> 0 < el >>> 0 ? 1 : 0)) | 0;
    h5l = h[11] = (h5l + fl) | 0;
    h[10] = (h5h + fh + (h5l >>> 0 < fl >>> 0 ? 1 : 0)) | 0;
    h6l = h[13] = (h6l + gl) | 0;
    h[12] = (h6h + gh + (h6l >>> 0 < gl >>> 0 ? 1 : 0)) | 0;
    h7l = h[15] = (h7l + hl) | 0;
    h[14] = (h7h + hh + (h7l >>> 0 < hl >>> 0 ? 1 : 0)) | 0;
  },
};

sjcl.hash.sha1 = function (hash) {
  if (hash) {
    this._h = hash._h.slice(0);
    this._buffer = hash._buffer.slice(0);
    this._length = hash._length;
  } else {
    this.reset();
  }
};

/**
 * Hash a string or an array of words.
 * @static
 * @param {bitArray|String} data the data to hash.
 * @return {bitArray} The hash value, an array of 5 big-endian words.
 */
sjcl.hash.sha1.hash = function (data) {
  return new sjcl.hash.sha1().update(data).finalize();
};

sjcl.hash.sha1.prototype = {
  /**
   * The hash's block size, in bits.
   * @constant
   */
  blockSize: 512,

  /**
   * Reset the hash state.
   * @return this
   */
  reset() {
    this._h = this._init.slice(0);
    this._buffer = [];
    this._length = 0;
    return this;
  },

  /**
   * Input several words to the hash.
   * @param {bitArray|String} data the data to hash.
   * @return this
   */
  update(data) {
    if (typeof data === 'string') {
      data = sjcl.codec.utf8String.toBits(data);
    }
    let i,
      b = (this._buffer = sjcl.bitArray.concat(this._buffer, data)),
      ol = this._length,
      nl = (this._length = ol + sjcl.bitArray.bitLength(data));
    for (i = (this.blockSize + ol) & -this.blockSize; i <= nl; i += this.blockSize) {
      this._block(b.splice(0, 16));
    }
    return this;
  },

  /**
   * Complete hashing and output the hash value.
   * @return {bitArray} The hash value, an array of 5 big-endian words. TODO
   */
  finalize() {
    let i,
      b = this._buffer,
      h = this._h;

    // Round out and push the buffer
    b = sjcl.bitArray.concat(b, [sjcl.bitArray.partial(1, 1)]);
    // Round out the buffer to a multiple of 16 words, less the 2 length words.
    for (i = b.length + 2; i & 15; i++) {
      b.push(0);
    }

    // append the length
    b.push(Math.floor(this._length / 0x100000000));
    b.push(this._length | 0);

    while (b.length) {
      this._block(b.splice(0, 16));
    }

    this.reset();
    return h;
  },

  /**
   * The SHA-1 initialization vector.
   * @private
   */
  _init: [0x67452301, 0xefcdab89, 0x98badcfe, 0x10325476, 0xc3d2e1f0],

  /**
   * The SHA-1 hash key.
   * @private
   */
  _key: [0x5a827999, 0x6ed9eba1, 0x8f1bbcdc, 0xca62c1d6],

  /**
   * The SHA-1 logical functions f(0), f(1), ..., f(79).
   * @private
   */
  _f(t, b, c, d) {
    if (t <= 19) {
      return (b & c) | (~b & d);
    } else if (t <= 39) {
      return b ^ c ^ d;
    } else if (t <= 59) {
      return (b & c) | (b & d) | (c & d);
    } else if (t <= 79) {
      return b ^ c ^ d;
    }
  },

  /**
   * Circular left-shift operator.
   * @private
   */
  _S(n, x) {
    return (x << n) | (x >>> (32 - n));
  },

  /**
   * Perform one cycle of SHA-1.
   * @param {bitArray} words one block of words.
   * @private
   */
  _block(words) {
    let t,
      tmp,
      a,
      b,
      c,
      d,
      e,
      w = words.slice(0),
      h = this._h;

    a = h[0];
    b = h[1];
    c = h[2];
    d = h[3];
    e = h[4];

    for (t = 0; t <= 79; t++) {
      if (t >= 16) {
        w[t] = this._S(1, w[t - 3] ^ w[t - 8] ^ w[t - 14] ^ w[t - 16]);
      }
      tmp = (this._S(5, a) + this._f(t, b, c, d) + e + w[t] + this._key[Math.floor(t / 20)]) | 0;
      e = d;
      d = c;
      c = this._S(30, b);
      b = a;
      a = tmp;
    }

    h[0] = (h[0] + a) | 0;
    h[1] = (h[1] + b) | 0;
    h[2] = (h[2] + c) | 0;
    h[3] = (h[3] + d) | 0;
    h[4] = (h[4] + e) | 0;
  },
};

/**
 * Hash a string using sha1.
 * @static
 * @param {base64 String} key the hash key.
 * @return {base64 String} The hash value, an array of 16 big-endian words.
 */
export const hmacSHA1 = function (key) {
  const hasher = new sjcl.misc.hmac(key, sjcl.hash.sha1);
  this.encrypt = function () {
    return hasher.encrypt.apply(hasher, arguments);
  };
};

/**
 * Hash a string using sha512 for define the hash algorithm of hmac.
 * @static
 * @param {base64 String} key the hash key.
 * @return {base64 String} The hash value.
 */
// out = (new sjcl.misc.hmac(key, sjcl.hash.sha256)).mac("The quick brown fox jumps over the lazy dog");
const _hmacSHA512 = function (key) {
  const hasher = new sjcl.misc.hmac(key, sjcl.hash.sha512);
  this.encrypt = function () {
    return hasher.encrypt.apply(hasher, arguments);
  };
};

/**
 * HMAC a string using sha512 with key.
 * @static
 * @param {utf8 String} key the hash key.
 * @param {utf8 String} data the hash key.
 * @return {base64 String} The hash value.
 */
export function hmacSHA512(key, data) {
  const out = new sjcl.misc.hmac(sjcl.codec.utf8String.toBits(key), sjcl.hash.sha512).encrypt(
    sjcl.codec.utf8String.toBits(data)
  );
  return sjcl.codec.base64.fromBits(out);
}

/**
 * Hash a string using sha512.
 * @static
 * @param {utf8 String} key the hash key.
 * @return {base64 String} The hash value.
 */
export const sha512 = function (key) {
  return sjcl.codec.base64.fromBits(sjcl.hash.sha512.hash(sjcl.codec.utf8String.toBits(key)));
};

/**
 * PBDFK2 a string of password.
 * @static
 * @param {utf8 String} password the data to hash.
 * @param {base64 String} key the hash key.
 * @param {Number} secureLevel secure level = 10 or 100.
 * @return {base64 String} The hash value, an array of 16 big-endian words.
 */
export function calculateSaltedPw(password, key, secureLevel) {
  let iteration;
  if (secureLevel === 10) {
    iteration = 10000;
  } else if (secureLevel === 20) {
    iteration = 20000;
  }
  const bitArray = sjcl.misc.pbkdf2(
    sjcl.codec.utf8String.toBits(password),
    sjcl.codec.base64.toBits(key),
    iteration,
    32 * 8,
    _hmacSHA512
  );
  // const str = CryptoJS.enc.Base64.stringify(CryptoJS.enc.Hex.parse(uint8Arr));
  // console.log('str:', str);
  return sjcl.codec.base64.fromBits(bitArray);
}

/**
 * bitwise exclusive-OR two base64 string.
 * @static
 * @param {base64 String} data1 the hash key.
 * @param {base64 String} data2 the hash key.
 * @return {base64 String} The hash value.
 */
export function bitwiseXOR(data1, data2) {
  return sjcl.codec.base64.fromBits(
    sjcl.codec.base64.toBits(data1).map((value, index) => {
      return value ^ sjcl.codec.base64.toBits(data2)[index];
    })
  );
}
/**
 * @static
 * @param {String} password
 * @param {Object} pwSalt the object of api.sysGetPwSalt.value
 * @param {String} salt
 * @param {String} nonce
 * @return {base64 String} The ClientProof value.
 */
export function calculateClientProof(password, pwSalt, salt, nonce) {
  const saltedPw = calculateSaltedPw(password, pwSalt.pwSalt, pwSalt.level);
  const clientKey = hmacSHA512(saltedPw, 'Client Key');
  const storeKey = sha512(clientKey);
  const clientSignature = hmacSHA512(storeKey, salt + nonce);
  const clientProof = bitwiseXOR(clientKey, clientSignature);
  return clientProof;
}

/*
please do: 
1. create nonce by `randomString` in this file
2. get salt by RESTful api
3. get pwsalt by RESTful api

using `calculateClientProof` to calcute client proof
the result will be the `auth` in RequestToken

    const info = {
      dev: devValue,  // javascript to get user agent type and version, here usually is "Chrome"
      swVer: '1.0',
      locale: 'zh_TW',
      cap: ['WebSocket'],
    };

    const nonce = randomString();
    const clientProof = calculateClientProof(password, pwSalt, salt, nonce);

    const resp = yield call(
      api.sysRequestToken,
      cid,
      dmid !== '' ? dmid : null,
      null, // fcmToken
      null, // apnsToken
      null, // deviceToken
      null, // authToken
      info, 
      pwsalt.level, //security level
      nonce,
      getSaltRlt.value.salt,  // result of GetSalt
      clientProof, 
      getIdentity(username)  // eg: { type: 'mobile', id: '886912345678' }
    );

    // the yield call will send a http post request with following data:

    {
      "cid":"1357a9c3-1dca-40ba-b31b-391177d560f9",
      "tag":4,
      "cmd": {
          "type":"RequestToken",
          "value":{
            "fcmToken":null,
            "apnsToken":null,
            "deviceToken":null,
            "authToken":null,
            "info":{"dev":{"type":"Browser","model":"Chrome","ver":"63.0.3239.132"},"swVer":"1.0","locale":"zh_TW","cap":["WebSocket"]},
            "securityLevel":10,
            "nonce":"4501b5272e724320a3888676b8dcc8f8455f043b",
            "salt":"IEQR9ssNC4W7c5g2n1ET",
            "auth":"5YJwybTyjKo8LK9nl9Gbsn4+c5fXnYp2K9UYqBXycSxPq5npv+MbzKU0olR2J6idoDRserdFyZbNW7niWGhTIg==",
            "id":{"type":"email","id":"2@new.com"}
          }
        }
    }
*/