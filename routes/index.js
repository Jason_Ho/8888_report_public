var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {
  // if(req.query.option == 'agree'){
  //   console.log('1')
  // }else{
  //   console.log('2')
  // }
  let pid = req.headers.pid;
  if (pid === undefined) {
    pid = '12345678';
  }
  res.render('index', {
    pid: pid,
    title: '8888 Report Complaints Center'
  });
});

/* POST home page. */
// router.post('/',function(req,res,next){
//   res.render('index', {
//     title: '8888 Report Complaints Center'
//   });
// })
module.exports = router;