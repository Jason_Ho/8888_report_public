var CryptoJS = require('crypto-js');
var sjcl = require('sjcl');
var Request = require('request');
var uuidBuffer = require('uuid-buffer');
var fs = require('fs');

exports.guid = function () {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  // return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
  return s4().concat(s4(), '-', s4(), '-', s4(), '-', s4(), '-', s4(), s4(), s4());
}

exports.isUUID = function (str) {
  function assertString(input) {
    if (typeof input !== 'string') {
      throw new TypeError('This library (validator.js) validates strings only');
    }
  }

  assertString(str);
  const pattern = /^[0-9A-F]{8}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{12}$/i;
  return pattern && pattern.test(str);
}

exports.hash256 = function (str) {
  /* eslint new-cap: ["error", { "properties": false }]*/
  return CryptoJS.enc.Base64.stringify(CryptoJS.SHA256(str));
}

exports.calculatePwHash = function (pw) {
  return this.hash256(pw);
}

exports.calculateAuth = function (pwHash, salt, nonce) {
  return this.hash256(pwHash + salt + nonce);
}

exports.randomString = function () {
  return CryptoJS.enc.Hex.stringify(CryptoJS.lib.WordArray.random(20));
}

exports.calculateSecret = function (uid, nonce, token) {
  return this.hash256(`nim:${uid}${nonce}${token}`);
}

exports.idIsEmail = function (id) {
  if (id.indexOf('@') !== -1) {
    // console.log('email');
    return true;
  }
  // console.log('mobile');
  return false;
}

exports.getIdentity = function (id) {
  if (id.indexOf('@') !== -1) {
    // console.log('email');
    return this.getEmailIdentity(id);
  }
  // console.log('mobile');
  return this.getPhoneIdentity(id);
}

exports.getEmailIdentity = function (email) {
  return { type: 'email', id: email };
}

exports.getPhoneIdentity = function (phone) {
  return { type: 'mobile', id: phone };
}

exports.getNameIdentity = function (name) {
  return { type: 'name', id: name };
}

/** @fileOverview Javascript SHA-512 implementation.
 *
 * This implementation was written for CryptoJS by Jeff Mott and adapted for
 * SJCL by Stefan Thomas.
 *
 * CryptoJS (c) 2009–2012 by Jeff Mott. All rights reserved.
 * Released with New BSD License
 *
 * @author Emily Stark
 * @author Mike Hamburg
 * @author Dan Boneh
 * @author Jeff Mott
 * @author Stefan Thomas
 */

/**
 * Context for a SHA-512 operation in progress.
 * @constructor
 */
sjcl.hash.sha512 = function (hash) {
  if (!this._key[0]) {
    this._precompute();
  }
  if (hash) {
    this._h = hash._h.slice(0);
    this._buffer = hash._buffer.slice(0);
    this._length = hash._length;
  } else {
    this.reset();
  }
};

/**
 * Hash a string or an array of words.
 * @static
 * @param {bitArray|String} data the data to hash.
 * @return {bitArray} The hash value, an array of 16 big-endian words.
 */
sjcl.hash.sha512.hash = function (data) {
  return new sjcl.hash.sha512().update(data).finalize();
};

sjcl.hash.sha512.prototype = {
  /**
   * The hash's block size, in bits.
   * @constant
   */
  blockSize: 1024,

  /**
   * Reset the hash state.
   * @return this
   */
  reset() {
    this._h = this._init.slice(0);
    this._buffer = [];
    this._length = 0;
    return this;
  },

  /**
   * Input several words to the hash.
   * @param {bitArray|String} data the data to hash.
   * @return this
   */
  update(data) {
    if (typeof data === 'string') {
      data = sjcl.codec.utf8String.toBits(data);
    }
    let i,
      b = (this._buffer = sjcl.bitArray.concat(this._buffer, data)),
      ol = this._length,
      nl = (this._length = ol + sjcl.bitArray.bitLength(data));
    if (nl > 9007199254740991) {
      throw new sjcl.exception.invalid('Cannot hash more than 2^53 - 1 bits');
    }

    if (typeof Uint32Array !== 'undefined') {
      const c = new Uint32Array(b);
      let j = 0;
      for (i = 1024 + ol - ((1024 + ol) & 1023); i <= nl; i += 1024) {
        this._block(c.subarray(32 * j, 32 * (j + 1)));
        j += 1;
      }
      b.splice(0, 32 * j);
    } else {
      for (i = 1024 + ol - ((1024 + ol) & 1023); i <= nl; i += 1024) {
        this._block(b.splice(0, 32));
      }
    }
    return this;
  },

  /**
   * Complete hashing and output the hash value.
   * @return {bitArray} The hash value, an array of 16 big-endian words.
   */
  finalize() {
    let i,
      b = this._buffer,
      h = this._h;

    // Round out and push the buffer
    b = sjcl.bitArray.concat(b, [sjcl.bitArray.partial(1, 1)]);

    // Round out the buffer to a multiple of 32 words, less the 4 length words.
    for (i = b.length + 4; i & 31; i++) {
      b.push(0);
    }

    // append the length
    b.push(0);
    b.push(0);
    b.push(Math.floor(this._length / 0x100000000));
    b.push(this._length | 0);

    while (b.length) {
      this._block(b.splice(0, 32));
    }

    this.reset();
    return h;
  },

  /**
   * The SHA-512 initialization vector, to be precomputed.
   * @private
   */
  _init: [],

  /**
   * Least significant 24 bits of SHA512 initialization values.
   *
   * Javascript only has 53 bits of precision, so we compute the 40 most
   * significant bits and add the remaining 24 bits as constants.
   *
   * @private
   */
  _initr: [0xbcc908, 0xcaa73b, 0x94f82b, 0x1d36f1, 0xe682d1, 0x3e6c1f, 0x41bd6b, 0x7e2179],

  /**
   * The SHA-512 hash key, to be precomputed.
   * @private
   */
  _key: [],

  /**
   * Least significant 24 bits of SHA512 key values.
   * @private
   */
  _keyr: [
    0x28ae22,
    0xef65cd,
    0x4d3b2f,
    0x89dbbc,
    0x48b538,
    0x05d019,
    0x194f9b,
    0x6d8118,
    0x030242,
    0x706fbe,
    0xe4b28c,
    0xffb4e2,
    0x7b896f,
    0x1696b1,
    0xc71235,
    0x692694,
    0xf14ad2,
    0x4f25e3,
    0x8cd5b5,
    0xac9c65,
    0x2b0275,
    0xa6e483,
    0x41fbd4,
    0x1153b5,
    0x66dfab,
    0xb43210,
    0xfb213f,
    0xef0ee4,
    0xa88fc2,
    0x0aa725,
    0x03826f,
    0x0e6e70,
    0xd22ffc,
    0x26c926,
    0xc42aed,
    0x95b3df,
    0xaf63de,
    0x77b2a8,
    0xedaee6,
    0x82353b,
    0xf10364,
    0x423001,
    0xf89791,
    0x54be30,
    0xef5218,
    0x65a910,
    0x71202a,
    0xbbd1b8,
    0xd2d0c8,
    0x41ab53,
    0x8eeb99,
    0x9b48a8,
    0xc95a63,
    0x418acb,
    0x63e373,
    0xb2b8a3,
    0xefb2fc,
    0x172f60,
    0xf0ab72,
    0x6439ec,
    0x631e28,
    0x82bde9,
    0xc67915,
    0x72532b,
    0x26619c,
    0xc0c207,
    0xe0eb1e,
    0x6ed178,
    0x176fba,
    0xc898a6,
    0xf90dae,
    0x1c471b,
    0x047d84,
    0xc72493,
    0xc9bebc,
    0x100d4c,
    0x3e42b6,
    0x657e2a,
    0xd6faec,
    0x475817,
  ],

  /**
   * Function to precompute _init and _key.
   * @private
   */
  _precompute() {
    // XXX: This code is for precomputing the SHA256 constants, change for
    //      SHA512 and re-enable.
    let i = 0,
      prime = 2,
      factor,
      isPrime;

    function frac(x) {
      return ((x - Math.floor(x)) * 0x100000000) | 0;
    }
    function frac2(x) {
      return ((x - Math.floor(x)) * 0x10000000000) & 0xff;
    }

    for (; i < 80; prime++) {
      isPrime = true;
      for (factor = 2; factor * factor <= prime; factor++) {
        if (prime % factor === 0) {
          isPrime = false;
          break;
        }
      }
      if (isPrime) {
        if (i < 8) {
          this._init[i * 2] = frac(Math.pow(prime, 1 / 2));
          this._init[i * 2 + 1] = (frac2(Math.pow(prime, 1 / 2)) << 24) | this._initr[i];
        }
        this._key[i * 2] = frac(Math.pow(prime, 1 / 3));
        this._key[i * 2 + 1] = (frac2(Math.pow(prime, 1 / 3)) << 24) | this._keyr[i];
        i++;
      }
    }
  },

  /**
   * Perform one cycle of SHA-512.
   * @param {Uint32Array|bitArray} words one block of words.
   * @private
   */
  _block(words) {
    let i,
      wrh,
      wrl,
      h = this._h,
      k = this._key,
      h0h = h[0],
      h0l = h[1],
      h1h = h[2],
      h1l = h[3],
      h2h = h[4],
      h2l = h[5],
      h3h = h[6],
      h3l = h[7],
      h4h = h[8],
      h4l = h[9],
      h5h = h[10],
      h5l = h[11],
      h6h = h[12],
      h6l = h[13],
      h7h = h[14],
      h7l = h[15];
    let w;
    if (typeof Uint32Array !== 'undefined') {
      // When words is passed to _block, it has 32 elements. SHA512 _block
      // function extends words with new elements (at the end there are 160 elements).
      // The problem is that if we use Uint32Array instead of Array,
      // the length of Uint32Array cannot be changed. Thus, we replace words with a
      // normal Array here.
      w = Array(160); // do not use Uint32Array here as the instantiation is slower
      for (let j = 0; j < 32; j++) {
        w[j] = words[j];
      }
    } else {
      w = words;
    }

    // Working variables
    let ah = h0h,
      al = h0l,
      bh = h1h,
      bl = h1l,
      ch = h2h,
      cl = h2l,
      dh = h3h,
      dl = h3l,
      eh = h4h,
      el = h4l,
      fh = h5h,
      fl = h5l,
      gh = h6h,
      gl = h6l,
      hh = h7h,
      hl = h7l;

    for (i = 0; i < 80; i++) {
      // load up the input word for this round
      if (i < 16) {
        wrh = w[i * 2];
        wrl = w[i * 2 + 1];
      } else {
        // Gamma0
        const gamma0xh = w[(i - 15) * 2];
        const gamma0xl = w[(i - 15) * 2 + 1];
        const gamma0h =
          ((gamma0xl << 31) | (gamma0xh >>> 1)) ^ ((gamma0xl << 24) | (gamma0xh >>> 8)) ^ (gamma0xh >>> 7);
        const gamma0l =
          ((gamma0xh << 31) | (gamma0xl >>> 1)) ^
          ((gamma0xh << 24) | (gamma0xl >>> 8)) ^
          ((gamma0xh << 25) | (gamma0xl >>> 7));

        // Gamma1
        const gamma1xh = w[(i - 2) * 2];
        const gamma1xl = w[(i - 2) * 2 + 1];
        const gamma1h =
          ((gamma1xl << 13) | (gamma1xh >>> 19)) ^ ((gamma1xh << 3) | (gamma1xl >>> 29)) ^ (gamma1xh >>> 6);
        const gamma1l =
          ((gamma1xh << 13) | (gamma1xl >>> 19)) ^
          ((gamma1xl << 3) | (gamma1xh >>> 29)) ^
          ((gamma1xh << 26) | (gamma1xl >>> 6));

        // Shortcuts
        const wr7h = w[(i - 7) * 2];
        const wr7l = w[(i - 7) * 2 + 1];

        const wr16h = w[(i - 16) * 2];
        const wr16l = w[(i - 16) * 2 + 1];

        // W(round) = gamma0 + W(round - 7) + gamma1 + W(round - 16)
        wrl = gamma0l + wr7l;
        wrh = gamma0h + wr7h + (wrl >>> 0 < gamma0l >>> 0 ? 1 : 0);
        wrl += gamma1l;
        wrh += gamma1h + (wrl >>> 0 < gamma1l >>> 0 ? 1 : 0);
        wrl += wr16l;
        wrh += wr16h + (wrl >>> 0 < wr16l >>> 0 ? 1 : 0);
      }

      w[i * 2] = wrh |= 0;
      w[i * 2 + 1] = wrl |= 0;

      // Ch
      const chh = (eh & fh) ^ (~eh & gh);
      const chl = (el & fl) ^ (~el & gl);

      // Maj
      const majh = (ah & bh) ^ (ah & ch) ^ (bh & ch);
      const majl = (al & bl) ^ (al & cl) ^ (bl & cl);

      // Sigma0
      const sigma0h = ((al << 4) | (ah >>> 28)) ^ ((ah << 30) | (al >>> 2)) ^ ((ah << 25) | (al >>> 7));
      const sigma0l = ((ah << 4) | (al >>> 28)) ^ ((al << 30) | (ah >>> 2)) ^ ((al << 25) | (ah >>> 7));

      // Sigma1
      const sigma1h = ((el << 18) | (eh >>> 14)) ^ ((el << 14) | (eh >>> 18)) ^ ((eh << 23) | (el >>> 9));
      const sigma1l = ((eh << 18) | (el >>> 14)) ^ ((eh << 14) | (el >>> 18)) ^ ((el << 23) | (eh >>> 9));

      // K(round)
      const krh = k[i * 2];
      const krl = k[i * 2 + 1];

      // t1 = h + sigma1 + ch + K(round) + W(round)
      let t1l = hl + sigma1l;
      let t1h = hh + sigma1h + (t1l >>> 0 < hl >>> 0 ? 1 : 0);
      t1l += chl;
      t1h += chh + (t1l >>> 0 < chl >>> 0 ? 1 : 0);
      t1l += krl;
      t1h += krh + (t1l >>> 0 < krl >>> 0 ? 1 : 0);
      t1l = (t1l + wrl) | 0; // FF32..FF34 perf issue https://bugzilla.mozilla.org/show_bug.cgi?id=1054972
      t1h += wrh + (t1l >>> 0 < wrl >>> 0 ? 1 : 0);

      // t2 = sigma0 + maj
      const t2l = sigma0l + majl;
      const t2h = sigma0h + majh + (t2l >>> 0 < sigma0l >>> 0 ? 1 : 0);

      // Update working variables
      hh = gh;
      hl = gl;
      gh = fh;
      gl = fl;
      fh = eh;
      fl = el;
      el = (dl + t1l) | 0;
      eh = (dh + t1h + (el >>> 0 < dl >>> 0 ? 1 : 0)) | 0;
      dh = ch;
      dl = cl;
      ch = bh;
      cl = bl;
      bh = ah;
      bl = al;
      al = (t1l + t2l) | 0;
      ah = (t1h + t2h + (al >>> 0 < t1l >>> 0 ? 1 : 0)) | 0;
    }

    // Intermediate hash
    h0l = h[1] = (h0l + al) | 0;
    h[0] = (h0h + ah + (h0l >>> 0 < al >>> 0 ? 1 : 0)) | 0;
    h1l = h[3] = (h1l + bl) | 0;
    h[2] = (h1h + bh + (h1l >>> 0 < bl >>> 0 ? 1 : 0)) | 0;
    h2l = h[5] = (h2l + cl) | 0;
    h[4] = (h2h + ch + (h2l >>> 0 < cl >>> 0 ? 1 : 0)) | 0;
    h3l = h[7] = (h3l + dl) | 0;
    h[6] = (h3h + dh + (h3l >>> 0 < dl >>> 0 ? 1 : 0)) | 0;
    h4l = h[9] = (h4l + el) | 0;
    h[8] = (h4h + eh + (h4l >>> 0 < el >>> 0 ? 1 : 0)) | 0;
    h5l = h[11] = (h5l + fl) | 0;
    h[10] = (h5h + fh + (h5l >>> 0 < fl >>> 0 ? 1 : 0)) | 0;
    h6l = h[13] = (h6l + gl) | 0;
    h[12] = (h6h + gh + (h6l >>> 0 < gl >>> 0 ? 1 : 0)) | 0;
    h7l = h[15] = (h7l + hl) | 0;
    h[14] = (h7h + hh + (h7l >>> 0 < hl >>> 0 ? 1 : 0)) | 0;
  },
};

sjcl.hash.sha1 = function (hash) {
  if (hash) {
    this._h = hash._h.slice(0);
    this._buffer = hash._buffer.slice(0);
    this._length = hash._length;
  } else {
    this.reset();
  }
};

/**
 * Hash a string or an array of words.
 * @static
 * @param {bitArray|String} data the data to hash.
 * @return {bitArray} The hash value, an array of 5 big-endian words.
 */
sjcl.hash.sha1.hash = function (data) {
  return new sjcl.hash.sha1().update(data).finalize();
};

sjcl.hash.sha1.prototype = {
  /**
   * The hash's block size, in bits.
   * @constant
   */
  blockSize: 512,

  /**
   * Reset the hash state.
   * @return this
   */
  reset() {
    this._h = this._init.slice(0);
    this._buffer = [];
    this._length = 0;
    return this;
  },

  /**
   * Input several words to the hash.
   * @param {bitArray|String} data the data to hash.
   * @return this
   */
  update(data) {
    if (typeof data === 'string') {
      data = sjcl.codec.utf8String.toBits(data);
    }
    let i,
      b = (this._buffer = sjcl.bitArray.concat(this._buffer, data)),
      ol = this._length,
      nl = (this._length = ol + sjcl.bitArray.bitLength(data));
    for (i = (this.blockSize + ol) & -this.blockSize; i <= nl; i += this.blockSize) {
      this._block(b.splice(0, 16));
    }
    return this;
  },

  /**
   * Complete hashing and output the hash value.
   * @return {bitArray} The hash value, an array of 5 big-endian words. TODO
   */
  finalize() {
    let i,
      b = this._buffer,
      h = this._h;

    // Round out and push the buffer
    b = sjcl.bitArray.concat(b, [sjcl.bitArray.partial(1, 1)]);
    // Round out the buffer to a multiple of 16 words, less the 2 length words.
    for (i = b.length + 2; i & 15; i++) {
      b.push(0);
    }

    // append the length
    b.push(Math.floor(this._length / 0x100000000));
    b.push(this._length | 0);

    while (b.length) {
      this._block(b.splice(0, 16));
    }

    this.reset();
    return h;
  },

  /**
   * The SHA-1 initialization vector.
   * @private
   */
  _init: [0x67452301, 0xefcdab89, 0x98badcfe, 0x10325476, 0xc3d2e1f0],

  /**
   * The SHA-1 hash key.
   * @private
   */
  _key: [0x5a827999, 0x6ed9eba1, 0x8f1bbcdc, 0xca62c1d6],

  /**
   * The SHA-1 logical functions f(0), f(1), ..., f(79).
   * @private
   */
  _f(t, b, c, d) {
    if (t <= 19) {
      return (b & c) | (~b & d);
    } else if (t <= 39) {
      return b ^ c ^ d;
    } else if (t <= 59) {
      return (b & c) | (b & d) | (c & d);
    } else if (t <= 79) {
      return b ^ c ^ d;
    }
  },

  /**
   * Circular left-shift operator.
   * @private
   */
  _S(n, x) {
    return (x << n) | (x >>> (32 - n));
  },

  /**
   * Perform one cycle of SHA-1.
   * @param {bitArray} words one block of words.
   * @private
   */
  _block(words) {
    let t,
      tmp,
      a,
      b,
      c,
      d,
      e,
      w = words.slice(0),
      h = this._h;

    a = h[0];
    b = h[1];
    c = h[2];
    d = h[3];
    e = h[4];

    for (t = 0; t <= 79; t++) {
      if (t >= 16) {
        w[t] = this._S(1, w[t - 3] ^ w[t - 8] ^ w[t - 14] ^ w[t - 16]);
      }
      tmp = (this._S(5, a) + this._f(t, b, c, d) + e + w[t] + this._key[Math.floor(t / 20)]) | 0;
      e = d;
      d = c;
      c = this._S(30, b);
      b = a;
      a = tmp;
    }

    h[0] = (h[0] + a) | 0;
    h[1] = (h[1] + b) | 0;
    h[2] = (h[2] + c) | 0;
    h[3] = (h[3] + d) | 0;
    h[4] = (h[4] + e) | 0;
  },
};

/**
 * Hash a string using sha1.
 * @static
 * @param {base64 String} key the hash key.
 * @return {base64 String} The hash value, an array of 16 big-endian words.
 */
exports.hmacSHA1 = function (key) {
  const hasher = new sjcl.misc.hmac(key, sjcl.hash.sha1);
  this.encrypt = function () {
    return hasher.encrypt.apply(hasher, arguments);
  };
};

/**
 * Hash a string using sha512 for define the hash algorithm of hmac.
 * @static
 * @param {base64 String} key the hash key.
 * @return {base64 String} The hash value.
 */
// out = (new sjcl.misc.hmac(key, sjcl.hash.sha256)).mac("The quick brown fox jumps over the lazy dog");
exports._hmacSHA512 = function (key) {
  const hasher = new sjcl.misc.hmac(key, sjcl.hash.sha512);
  this.encrypt = function () {
    return hasher.encrypt.apply(hasher, arguments);
  };
};

/**
 * HMAC a string using sha512 with key.
 * @static
 * @param {utf8 String} key the hash key.
 * @param {utf8 String} data the hash key.
 * @return {base64 String} The hash value.
 */
exports.hmacSHA512 = function (key, data) {
  const out = new sjcl.misc.hmac(sjcl.codec.utf8String.toBits(key), sjcl.hash.sha512).encrypt(
    sjcl.codec.utf8String.toBits(data)
  );
  return sjcl.codec.base64.fromBits(out);
}

/**
 * Hash a string using sha512.
 * @static
 * @param {utf8 String} key the hash key.
 * @return {base64 String} The hash value.
 */
exports.sha512 = function (key) {
  return sjcl.codec.base64.fromBits(sjcl.hash.sha512.hash(sjcl.codec.utf8String.toBits(key)));
};

/**
 * PBDFK2 a string of password.
 * @static
 * @param {utf8 String} password the data to hash.
 * @param {base64 String} key the hash key.
 * @param {Number} secureLevel secure level = 10 or 100.
 * @return {base64 String} The hash value, an array of 16 big-endian words.
 */
exports.calculateSaltedPw = function (password, key, secureLevel) {
  let iteration;
  if (secureLevel === 10) {
    iteration = 10000;
  } else if (secureLevel === 20) {
    iteration = 20000;
  }
  const bitArray = sjcl.misc.pbkdf2(
    sjcl.codec.utf8String.toBits(password),
    sjcl.codec.base64.toBits(key),
    iteration,
    32 * 8,
    this._hmacSHA512
  );
  // const str = CryptoJS.enc.Base64.stringify(CryptoJS.enc.Hex.parse(uint8Arr));
  // console.log('str:', str);
  return sjcl.codec.base64.fromBits(bitArray);
}

/**
 * bitwise exclusive-OR two base64 string.
 * @static
 * @param {base64 String} data1 the hash key.
 * @param {base64 String} data2 the hash key.
 * @return {base64 String} The hash value.
 */
exports.bitwiseXOR = function (data1, data2) {
  return sjcl.codec.base64.fromBits(
    sjcl.codec.base64.toBits(data1).map((value, index) => {
      return value ^ sjcl.codec.base64.toBits(data2)[index];
    })
  );
}

/**
 * @static
 * @param {String} password
 * @param {Object} pwSalt the object of api.sysGetPwSalt.value
 * @param {String} salt
 * @param {String} nonce
 * @return {base64 String} The ClientProof value.
 */
exports.calculateClientProof = function (password, pwSalt, salt, nonce) {
  const saltedPw = this.calculateSaltedPw(password, pwSalt.pwSalt, pwSalt.level);
  const clientKey = this.hmacSHA512(saltedPw, 'Client Key');
  const storeKey = this.sha512(clientKey);
  const clientSignature = this.hmacSHA512(storeKey, salt + nonce);
  const clientProof = this.bitwiseXOR(clientKey, clientSignature);
  return clientProof;
}

exports.getSalt = function (typecode, callbak) {
  let url = "";
  if (typecode == 1) {
    url = "http://loop.wattertek.com/rest/sys";
  } else {
    url = "http://ph1.wattertek.com/rest/sys";
  }
  Request.post({
    headers: { 'Content-Type': 'application/json' },
    url: url,
    body: '{"cid":"4b7cc569-4dd3-3265-bac3-26eaba31266e","tag":103,"cmd":{"type":"GetSalt", "value":{}}}'
  }, function (error, response, body) {
    if (error == null) {
      let obj = JSON.parse(body);
      callbak(null, obj.rlt.value.salt);
    } else {
      callbak(error, null);
    }
  });
}

exports.getPwSalt = function (typecode, type, id, callbak) {
  let url = "";
  if (typecode == 1) {
    url = "http://loop.wattertek.com/rest/sys";
  } else {
    url = "http://ph1.wattertek.com/rest/sys";
  }
  Request.post({
    headers: { 'Content-Type': 'application/json' },
    url: url,
    body: '{"cid":"4b7cc569-4dd3-3265-bac3-26eaba31266e","tag":103,"cmd":{"type":"GetPwSalt", "value":{"id":{"type":"' + type + '","id":"' + id + '"}}}}'
  }, function (error, response, body) {
    if (error == null) {
      let obj = JSON.parse(body);
      callbak(null, obj.rlt.value);
    } else {
      callbak(error, null);
    }
  });
}

exports.getToken = function (typecode, Id, pwd, pwsalt, salt, callbak) {
  let url = "";
  let dmid = "";
  if (typecode == 1) {
    url = "http://loop.wattertek.com:8080/rest/sys";
  } else {
    url = "http://ph1.wattertek.com:8080/rest/sys";
  }
  let nonce = this.randomString();
  let clientproof = this.calculateClientProof(pwd, pwsalt, salt, nonce);
  let query = '{\
    "cid": "4b7cc569-4dd3-3265-bac3-26eaba31266e",\
    "tag": 108,\
    "cmd": {\
        "type": "RequestToken",\
        "value": {\
        "nonce": "'+ nonce + '",\
        "dmid": "c21f969b-5f03-433d-95e0-4f8f136e7682",\
        "auth": "'+ clientproof + '",\
        "info": {\
            "dev": {\
            "type": "Browser",\
            "model": "Chrome",\
            "ver": "63.0.3239.132"\
            },\
            "swVer": "1.0",\
            "locale": "zh_TW",\
            "cap": ["WebSocket"]\
        },\
        "securityLevel": 10,\
        "id": {\
            "type": "mobile",\
            "id": "'+ Id + '"\
        },\
        "salt": "'+ salt + '"\
        }\
      }\
    }';
  Request.post({
    headers: { 'Content-Type': 'application/json' },
    url: url,
    body: query
  }, function (error, response, body) {
    if (error == null) {
      let obj = JSON.parse(body);
      callbak(null, obj.rlt.value.serverKey, obj.rlt.value.token, obj.rlt.value.uid);
    } else {
      callbak(error, null, null, null);
    }
  })
}

exports.doFollowAgent = function (typecode, uid, accesstoken, pid, agid, callbak) {
  let url = "";
  if (typecode == 1) {
    url = "http://loop.wattertek.com/rest/usr";
  } else {
    url = "http://ph1.wattertek.com/rest/usr";
  }
  let str = '{\
    "uid": "'+ uid + '",\
    "tok": "'+ accesstoken + '",\
      "req": {\
        "type": "PFL",\
        "value": {\
            "cid": "4b7cc569-4dd3-3265-bac3-26eaba31266e",\
            "tag": 261,\
            "pid": "'+ pid + '",\
            "cmd": {\
                "type": "AddFriend",\
                "value": {\
                  "pid": "'+ agid + '",\
                  "msg": "Please accept my invitation!",\
                  "privacy": ["MSG", "MNT", "INFO"]\
                }\
            }\
        }\
    }\
  }';
  Request.post({
    headers: { 'Content-Type': 'application/json' },
    url: url,
    body: str
  }, function (error, response, body) {
    if (error == null) {
      callbak(null, body);
    } else {
      callbak(error, null);
    }
  })
}

/**
 * Send Text Message
 * @param {int} typecode 1 => loop, 2 => ph1
 * @param {uuid} uid send message user id
 * @param {string} accesstoken send message user token
 * @param {uuid} pid send message user pid
 * @param {uuid} peer agent id
 * @param {string} content message
 * @return callbak(error,body,response)
 **/
exports.doSendMsg = function (typecode, uid, accesstoken, pid, peer, content, callbak) {
  let url = "";
  if (typecode == 1) {
    url = "http://loop.wattertek.com/rest/usr";
  } else {
    url = "http://ph1.wattertek.com/rest/usr";
  }
  let msg = '{\
    "uid": "'+ uid + '",\
    "tok": "'+ accesstoken + '",\
    "req": {\
        "type": "MSG",\
        "value": {\
              "cid": "4b7cc569-4dd3-3265-bac3-26eaba31266e",\
              "tag": 344,\
              "pid": "'+ pid + '",\
              "cmd": {\
                  "type": "SendMsg",\
                  "value": {\
                  "peer": "'+ peer + '",\
                  "body": {\
                          "type": "Text",\
                          "value": "'+ content + '"\
                      }\
                  }\
              }\
        }\
    }\
  }';
  Request.post({
    headers: { 'Content-Type': 'application/json' },
    url: url,
    body: msg
  }, function (error, response, body) {
    if (error == null) {
      callbak(null, body, response);
    } else {
      callbak(error, null, null);
    }
  });
}

/*
 * 取得自己建立的Agent清單
 */
exports.doGetMyAgentList = function (typecode, uid, accesstoken, callbak) {
  let url = "";
  if (typecode == 1) {
    url = "http://loop.wattertek.com/rest/usr";
  } else {
    url = "http://ph1.wattertek.com/rest/usr";
  }
  let msg = '{\
	  "uid": "'+ uid + '",\
	  "tok": "'+ accesstoken + '",\
	  "req": {\
	    "type": "USR",\
	    "value": {\
	      "cid": "4b7cc569-4dd3-3265-bac3-26eaba31266e",\
	      "tag": 161,\
	      "cmd": {\
	        "type": "GetAgentList",\
	        "value": {\
	          "limit": 10\
	        }\
	      }\
	    }\
	  }\
	}';
  Request.post({
    headers: { 'Content-Type': 'application/json' },
    url: url,
    body: msg
  }, function (error, response, body) {
    if (error == null) {
      callbak(null, body, response);
    } else {
      callbak(error, null, null);
    }
  });
}

/*
 * 取得尚未加入而可以加入的Agent清單
 */
exports.doGetAvailableAgentList = function (typecode, uid, accesstoken, pid, callbak) {
  let url = "";
  if (typecode == 1) {
    url = "http://loop.wattertek.com/rest/usr";
  } else {
    url = "http://ph1.wattertek.com/rest/usr";
  }
  let msg = '{\
	  "uid": "'+ uid + '",\
	  "tok": "'+ accesstoken + '",\
	  "req": {\
	    "type": "PFL",\
	    "value": {\
	      "cid": "4b7cc569-4dd3-3265-bac3-26eaba31266e",\
	      "tag": 289,\
	      "pid": "'+ pid + '",\
	      "cmd": {\
	        "type": "GetAvailableAgents",\
	        "value": {\
	          "limit": 100,\
	          "start": 0,\
	          "locale": "zh_TW"\
	        }\
	      }\
	    }\
	  }\
	}';
  Request.post({
    headers: { 'Content-Type': 'application/json' },
    url: url,
    body: msg
  }, function (error, response, body) {
    if (error == null) {
      let agents = JSON.parse(body).rsp.value.rlt.value.rlt;
      callbak(null, body, agents, response);
    } else {
      callbak(error, null, null, null);
    }
  });
}

/*
 * 判斷id類別
 */
exports.getUUIDType = function (uuidStr) {
  const data = uuidBuffer.toBuffer(uuidStr);
  let lsb = 0;
  lsb = (lsb << 8) | (data[8] & 0xff);
  switch (lsb & 0x3f) {
    case 3: // user id
      return 'uid';
    case 5: // profile id
      return 'pid';
    case 7: // group id
      return 'gid';
    case 11:
      return 'vid';
    case 9:
      return 'sid';
    case 13: // robot id
      return 'rid';
    case 17: // device id
      return 'did';
    case 19: // admin id
      return 'aid';
    case 21: // domain id
      return 'dmid';
    case 23: // agent id
      return 'agid';
    case 29: // application id
      return 'appid';
    case 31:
      return 'confid';
    default:
      return 'unknown';
  }
}

/**
 * Send Image file
 * @param {int} typecode 1 => loop, 2 => ph1
 * @param {uuid} uid send message user id
 * @param {string} accesstoken send message user token
 * @param {uuid} agid send message user pid
 * @param {stream} file image file
 * @return callbak(error,body,response)
 */
exports.doSendImage = function (typecode, uid, accesstoken, pid, agid, file, callbak) {
  let url = "";
  if (typecode == 1) {
    url = "http://loop.wattertek.com/rest/usr/upload";
  } else {
    url = "http://ph1.wattertek.com/rest/usr/upload";
  }
  let msg = '{\
	  "uid": "'+ uid + '",\
	  "tok": "'+ accesstoken + '",\
	  "req": {\
	    "type": "MSG",\
	    "value": {\
	      "cid": "4b7cc569-4dd3-3265-bac3-26eaba31266e",\
	      "tag": 374,\
	      "pid": "'+ pid + '",\
	      "cmd": {\
	        "type": "SendMsg",\
	        "value": {\
	          "peer": "'+ agid + '",\
	          "body": {\
	            "type": "Image"\
	          }\
	        }\
	      }\
	    }\
	  }\
	}';
  var formData = {
    'json': msg,
    'file': file
  };
  Request.post({
    headers: { 'Content-Type': 'multipart/form-data' },
    url: url,
    formData: formData
  }, function (error, response, body) {
    if (error == null) {
      callbak(null, body, response);
    } else {
      callbak(error, null, null);
    }
  });
}

exports.GetAppAccessToken = function(typecode, appid, appkey, agid, scope, callback){
  
  let url = "";
  if (typecode == 1) {
    url = "http://loop.wattertek.com/provider/token";
  } else if(typecode == 2) {
    url = "http://ph1.wattertek.com/provider/token";
  } else {
    url = "http://192.168.100.153:8080/provider/token"
  }
  let auth = "Basic " + new Buffer(appid + ":" + appkey).toString("base64");
  // console.log(auth)
  let Qs = {
    "agent_id": agid,
    "domain_id": 'c21f969b-5f03-433d-95e0-4f8f136e7682',
    "scope": scope,
    "grant_type": "client_credentials"
  };
  // console.log(Qs)
  Request.post({
    headers: { 'Authorization': auth },
    url: url,
    qs: Qs
  }, function (error, response, body) {
    if (error == null) {
      console.log("body:"+body)
      callback(null, body, response);
    } else {
      console.log("2")
      callback(error, null, null);
    }
  });
}

exports.GetAppTicket = function(typecode, accesstoken, callback){
  let url = "";
  if (typecode == 1) {
    url = "http://loop.wattertek.com/access";
  } else if(typecode == 2) {
    url = "http://ph1.wattertek.com/access";
  } else {
    url = "http://192.168.100.153:8080/access"
  }
  let Body = '{"query":"query { callCenterScope { ticket }}"}';
  let auth = "Bearer " + accesstoken;
  Request.post({
    headers: { 'Authorization': auth, 'Content-Type': 'application/json'},
    url: url,
    body: Body
  }, function (error, response, body) {
    if (error == null) {
      callback(null, body, response);
    } else {
      callback(error, null, null);
    }
  });
}

/**
 * 回覆訊息給使用者
 * @param {number} typecode 1.loop 2.PH1
 * @param {uuid} accesstoken 認證用accessToken
 * @param {uuid} pid 
 * @param {string} msg 
 * @param {function (error, response, body)} callback 
 */
exports.doPushMessage = function(typecode, accesstoken, pid, msg, callback){
  // console.log(pid);
  let url = "";
  if (typecode == 1) {
    url = "http://loop.wattertek.com/access";
  }else{
    url = "http://ph1.wattertek.com/access";
  }
  let Body = '{"query": "mutation{pushMessageScope{pushMsg(Message:{to:\\"' + pid + '\\" ,messages:[{type:Text,value: \\"' + msg + '\\" }]})}}"}';
  let auth = "Bearer " + accesstoken;

  Request.post({
    headers: { 'Authorization': auth, 'Content-Type': 'application/json'},
    url: url,
    body: Body
  }, function (error, response, body) {
    if (error == null) {
      callback(null, body, response);
      console.log("bodymessage ="+body)
      console.log("response message = ",response)
    } else {
      callback(error, null, null);
    }
  });
}