const express = require('express');
let router = express.Router();
const logger = require('morgan');
var request = require('request');

/* GET home page. */
router.get('/', function (req, res, next) {
    let pid = req.headers.pid;

    if (pid === undefined) {
        pid = req.body.pid;
    }
    if (pid === undefined) {
        pid = '12345678';
    }


    if (pid === undefined || pid === '') {
        res.render('query', {
            info: 'No History Reports',
            rows: {}
        });
    } else {
        //172.1.2.7測試站環境
        let searchapi = 'http://172.1.2.7:8080/ArcareEng/ProjectQueryService'
        let body = {
            strProjectId: '{C7E68572-BF70-0001-3C5A-148077023840}',
            strCompanyId: '{C7E68593-F1D0-0001-6B25-5DCA14601CA9}',
            strApiId: 'Ticket_list',
            oParamValue: {
                "pid": pid
            }
        }

        //正式環境
        // let searchapi = 'http://localhost:8080/ArcareEng/ProjectQueryService'
        // let body = {
        //   strProjectId: '{C7EBA889-1330-0001-4CB5-1E70B38E18D1}',
        //   strCompanyId: '{C7EBA8A5-D790-0001-BFAB-26565D0F1AA8}',
        //   strApiId: 'Ticket_list',
        //   oParamValue: {
        //     "pid": pid
        //   }
        // }

        request.post({
            headers: {
                'Content-Type': 'text/plain;charset=UTF-8'
            },
            url: searchapi,
            form: JSON.stringify(body)
        }, function (error, response, body) {
            let bSuccess = JSON.parse(body).bSuccess
            if (bSuccess == false) {
                console.log(JSON.parse(body))
            } else {
                console.log('Get ticketid query result success !')

                let results = JSON.parse(body).record;
                results.map((ele, index) => {
                    ele.NO = index + 1
                })

                console.log(results)


                res.render('query', {
                    info: 'Select one to see the detail',
                    rows: results
                });

            }

        });

    }
});

/* POST home page. */
router.post('/', function (req, res, next) {
    let pid = req.body.pid;

    if (pid === undefined || pid === '') {
        res.render('query', {
            info: 'No History Reports',
            rows: {}
        });
    } else {
        //172.1.2.7測試站環境
        let searchapi = 'http://172.1.2.7:8080/ArcareEng/ProjectQueryService'
        let body = {
            strProjectId: '{C7E68572-BF70-0001-3C5A-148077023840}',
            strCompanyId: '{C7E68593-F1D0-0001-6B25-5DCA14601CA9}',
            strApiId: 'Ticket_list',
            oParamValue: {
                "pid": pid
            }
        }

        // //正式站環境
        // let searchapi = 'http://localhost:8080/ArcareEng/ProjectQueryService'
        // let body = {
        //   strProjectId: '{C7EBA889-1330-0001-4CB5-1E70B38E18D1}',
        //   strCompanyId: '{C7EBA8A5-D790-0001-BFAB-26565D0F1AA8}',
        //   strApiId: 'Ticket_list',
        //   oParamValue: {
        //     "pid": pid
        //   }
        // }

        request.post({
            headers: {
                'Content-Type': 'text/plain;charset=UTF-8'
            },
            url: searchapi,
            form: JSON.stringify(body)
        }, function (error, response, body) {
            let resval = JSON.parse(body).bSuccess;
            if (resval == true) {
                console.log('Get ticketid query result success !')
            } else {
                console.log(body)
            }

            let results = JSON.parse(body).record;
            results.map((ele, index) => {
                ele.NO = index + 1
            })


            res.render('query', {
                info: 'Select one to see the detail',
                rows: results
            });
        });


    }
});
module.exports = router;