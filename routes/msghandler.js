const fs = require('fs')
const AWS = require('aws-sdk');
AWS.config.loadFromPath('./config.json');
const s3 = new AWS.S3();
const nodemailer = require('nodemailer');
const agrobot = require('./agrobot');
var request = require('request');

exports.Message = function (msgInfo) {
    this.msgInfo = msgInfo

    this.seeVar = function () {
        console.log(this.msgInfo)
    }
    this.saveToS3 = function () {
        let imgs = this.msgInfo.imgs

        return new Promise((resolve, reject) => {

            if (imgs.length != 0) {
                let params, counts = 0
                imgs.forEach(img => {
                    params = {
                        Bucket: 'project-crm',
                        Key: img.filename,
                        ContentType: 'text/plain',
                        Body: fs.readFileSync(img.path)
                    }
                    s3.upload(params, (err, data) => {
                        if (err) {
                            reject(err)
                        } else {
                            console.log(`Upload ${img.filename} to AWS/S3 finished`)
                            counts += 1

                            // 如果最後一個檔案上傳完畢則resolve
                            if (counts === imgs.length) {
                                console.log('All finished')
                                resolve()
                            }
                        }
                    })
                })
            } else {
                console.log('No image need to upload')
                resolve()
            }
        })
        // return new Promise((resolve, reject) => {

        //     if (this.msgInfo.hasImg) {
        //         let params = {
        //             Bucket: 'project-crm',
        //             Key: this.msgInfo.msgBody.imgname,
        //             ContentType: 'text/plain',
        //             Body: fs.readFileSync(this.msgInfo.imgs[0].path)
        //         };
        //         s3.upload(params, function (err, data) {
        //             if (err) {
        //                 console.log(err)
        //                 reject(err);
        //             } else {
        //                 console.log('Upload to AWS/S3 finished');
        //                 resolve();
        //             }
        //         })
        //     } else {
        //         console.log('No image need to upload')
        //         resolve();
        //     }
        // })
    }
    this.sendToRobot = function () {
        // find agid
        let fileStreams = [],
            imgs = this.msgInfo.imgs,
            pushMsg = this.msgInfo.msgBody,
            dt = this.msgInfo.datetime,
            agid,
            msgString = `{New Complaint Notification}  \\n  [Report time] : ${dt}   \\n  [User name] : ${pushMsg.name}   \\n  [Mobile] : ${pushMsg.mobile}   \\n  [Main complaint category] : ${pushMsg.maincat}   \\n  [Sub complaint category] : ${pushMsg.subcat}   \\n  [Responsible department] : ${pushMsg.dept}   \\n  [Content] : ${pushMsg.mcontent}  `;

        if (imgs.length != 0) {
            imgs.forEach(img => {
                fileStreams.push(fs.createReadStream(img.path))
            })
        } else {
            fileStreams = []
        }

        // let fileStream;
        // let pushMsg = this.msgInfo.msgBody
        // let dt = this.msgInfo.datetime
        let msgdept = this.msgInfo.msgBody.dept

        // if (this.msgInfo.hasImg) {
        //     fileStream = fs.createReadStream(this.msgInfo.imgs[0].path)
        //     resolve();
        // } else {
        //     fileStream = undefined;
        //     resolve();
        // }

        // 去acare robot db取agid
        //172.1.2.7測試站環境
        let searchapi = ''
        let body = {
            strProjectId: '{}',
            strCompanyId: '{}',
            strApiId: 'V_Agid',
            oParamValue: {
                "dept": msgdept
            }
        }

        // //正式站環境
        // let searchapi = ''
        // let body = {
        //     strProjectId: '{}',
        //     strCompanyId: '{}',
        //     strApiId: 'V_Agid',
        //     oParamValue: {
        //         "dept": msgdept
        //     }
        // }

        return new Promise((resolve, reject) => {
            request.post({
                headers: {
                    'Content-type': 'application/x-www-form-urlencoded'
                },
                url: searchapi,
                form: JSON.stringify(body)
            }, function (error, response, body) {

                if (error) reject(error)
                else {
                    let results = JSON.parse(body).record;
                    let deptrepert = [];

                    results.forEach(function (element) {
                        deptrepert.push(element.AGENCY_NO)
                    });

                    let dept = Array.from(new Set(deptrepert));
                    let agid = dept[0];

                    // let msgString = `{New Complaint Notification}  \\n  [Report time] : ${dt}   \\n  [User name] : ${pushMsg.name}   \\n  [Mobile] : ${pushMsg.mobile}   \\n  [Main complaint category] : ${pushMsg.maincat}   \\n  [Sub complaint category] : ${pushMsg.subcat}   \\n  [Responsible department] : ${pushMsg.dept}   \\n  [Content] : ${pushMsg.mcontent}  `;
                    agrobot.send(agid, msgString, fileStreams);
                    resolve()
                }
            })
        })
    }

    this.sendMail = function () {
        let imgs = this.msgInfo.imgs
        let pushMsg = this.msgInfo.msgBody
        let dt = this.msgInfo.datetime

        let transporter = nodemailer.createTransport({
            service: 'Gmail',
            auth: {
                user: 'dev@wattertek.com',
                pass: 'Wtk-1101'
            }
        });

        // Set nodemailer options
        let options = {
            from: 'sys@wattertek.com', //寄件者
            to: '', //收件者
            // cc: 'tea.lin@wattertek.com', //副本
            //bcc: 'wattertech.inc@gmail.com;dagodalin@gmail.com', //密件副本
            subject: '8888 report from : ' + pushMsg.name, //主旨
            //嵌入 html 的內文
            html: `
                <h3>The following is the information of a complaint reported from the LOOP IM App:</h3>
                <h4>Reported Time : </h4>
                <p style="color:black;margin:20px;">  ${dt} </p>
                <h4>User name : </h4>
                <p style="color:black;margin:20px;">  ${pushMsg.name} </p>
                <h4>Mobile : </h4>
                <p style="color:black;margin:20px;">  ${pushMsg.mobile} </p>
                <h4>Main Category : </h4>
                <p style="color:black;margin:20px;">  ${pushMsg.maincat} </p>
                <h4>Sub Category : </h4>
                <p style="color:black;margin:20px;">  ${pushMsg.subcat} </p>
                <h4>Resposible Department Towarded : </h4>
                <p style="color:black;margin:20px;">  ${pushMsg.dept} </p>
                <h4>Description of the complaint : </h4>
                <p style="color:black;margin:20px;">  ${pushMsg.mcontent} </p>
                <br>
                <p></p>
                <h5>Regards</h5>
                `
        };

        // if (this.msgInfo.hasImg) {
        //     options.attachments = this.msgInfo.imgs
        // }

        if (imgs.length != 0) {
            options.attachments = imgs
        }

        return new Promise((resolve, reject) => {
            transporter.sendMail(options, function (err, info) {
                if (err) {
                    console.log(err);
                    reject(err);
                } else {
                    console.log('Send mail finished');
                    resolve();
                }
            });

        })
        // transporter.sendMail(options, function (err, info) {
        //     if (err) {
        //         console.log(err);
        //     } else {
        //         console.log('Send mail finished');
        //     }
        // });
    }
    this.cleanTmp = function () {

        let imgs = this.msgInfo.imgs,
            counts = 0

        return new Promise((resolve, reject) => {

            if (imgs.length != 0) {
                imgs.forEach(img => {
                    fs.unlinkSync(img.path)
                    console.log(`Removed file: ${img.path}`)
                    counts += 1

                    if (counts === imgs.length) {
                        console.log('/tmp is cleaned')
                        resolve()
                    }
                })
            } else {
                console.log('No image need to clean')
                resolve()
            }

            // if (this.msgInfo.imgs[0] != undefined) {
            //     fs.unlinkSync(this.msgInfo.imgs[0].path)
            //     console.log("Img clean");
            //     resolve();
            // } else {
            //     console.log("No img need clean");
            //     resolve();
            // }

        })
    }
    this.inserttoarcare = function () {

        let imgs = this.msgInfo.imgs,
            msgBody = this.msgInfo.msgBody
            
        // insert in to s3
        if (imgs.length != 0) {
            msgBody.imgname = imgs.map(img => '' + img.filename).join(';').split(';');
            console.log(msgBody.imgname);
            console.log(msgBody)
        } else {
            msgBody.imgname = undefined
        }
        console.log(msgInfo.msgBody.imgname);
        if(msgInfo.msgBody.imgname == undefined){
            msgInfo.msgBody.imgname = [];

        }

        // let seturl;
        // if (msgInfo.msgBody.imgname == null) {
        //     msgInfo.msgBody.imgname = seturl;
        // } else {
        //     msgInfo.msgBody.imgname = '' + msgInfo.msgBody.imgname;
        // }

        //測試站環境
        let robotapi = '';
        let body = {
            strProjectId: '{}',
            nLanguageId: 950,
            strFuncPdno: '',
            strCompanyId: '{}',
            // //正式站環境
            // let robotapi = '';
            // let body = {
            //     strProjectId: '{}',
            //     nLanguageId: 950,
            //     strFuncPdno: '',
            //     strCompanyId: '{}',
            oUserDefParam: {
                ticket_id: msgInfo.msgBody.ticket_id,
                pid: msgInfo.msgBody.pid,
                name: msgInfo.msgBody.name,
                mobile: msgInfo.msgBody.mobile,
                mcate: msgInfo.msgBody.maincat,
                scate: msgInfo.msgBody.subcat,
                dept: msgInfo.msgBody.dept,
                content: msgInfo.msgBody.mcontent,
                manpower:msgInfo.msgBody.manpower,
                area:msgInfo.msgBody.area,
                url: msgInfo.msgBody.imgname[0],
                file_2:msgInfo.msgBody.imgname[1],
                file_3:msgInfo.msgBody.imgname[2],
                file_4:msgInfo.msgBody.imgname[3],
                file_5:msgInfo.msgBody.imgname[4],
                file_6:msgInfo.msgBody.imgname[5],
                file_7:msgInfo.msgBody.imgname[6],
                file_8:msgInfo.msgBody.imgname[7],
                file_9:msgInfo.msgBody.imgname[8],
            },
            strAccount: '',
            strPassword: ''

        }
        console.log(body.oUserDefParam)
        return new Promise((resolve, reject) => {

            request.post({
                headers: {
                    'Content-type': 'application/x-www-form-urlencoded'
                },
                url: robotapi,
                form: JSON.stringify(body)
            }, function (error, response, body) {
                if (error) {
                    console.log(error);
                    reject(error);
                }
                //回傳值判斷
                let resval = JSON.parse(body).bSuccess;
                if (resval == true) {
                    console.log('Insert to DB success!');
                    resolve();
                } else {
                    console.log('Insert to DB fail!');
                    // console.log(body);
                    reject(body);
                }
            });
        });
    }

    this.msghander = async function () {
        await this.saveToS3()
        // await this.sendToRobot() 
        await this.inserttoarcare().catch(error => console.log(error))
        // await this.sendMail()
        await this.cleanTmp();
    }
}