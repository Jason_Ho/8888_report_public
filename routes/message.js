const fs = require('fs')
const express = require('express');
let router = express.Router();
const logger = require('morgan');
const msghandler = require('./msghandler');
var request = require('request')
var multer = require('multer')
// var FormParser = multer({
//     dest: '/tmp/',
//     preservePath: true
// })

// 定義form parser儲存位置以及檔名格式
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, '/tmp/')
    },
    filename: function (req, file, cb) {
        cb(null, `${Date.now()}.${file.originalname.split('.').pop()}`)
    }
})
// 建立multer()物件
const FormParser = multer({
    preservePath: true,
    storage: storage
})

/* GET home page. */
router.get('/', function (req, res, next) {
    //console.log(req);
    let ticnum = req.query.TICKET_ID;
    console.log(ticnum);

    /* search message number api*/
    // //172.1.2.7 測試站環境
    let searchapi = 'http://172.1.2.7:8080/ArcareEng/ProjectQueryService'
    let body = {
        strProjectId: '{C7E68572-BF70-0001-3C5A-148077023840}',//arcare projectID
        strCompanyId: '{C7E68593-F1D0-0001-6B25-5DCA14601CA9}',//arcare companyID
        strApiId: 'V_Message_List',//API ID
        oParamValue: {
            "ticnum": ticnum
        }
    }

    //AWS正式站環境

    // let searchapi = 'http://localhost:8080/ArcareEng/ProjectQueryService'
    // let body = {
    //   strProjectId: '{C7EBA889-1330-0001-4CB5-1E70B38E18D1}',//arcare projectID
    //   strCompanyId: '{C7EBA8A5-D790-0001-BFAB-26565D0F1AA8}',//arcare companyID
    //   strApiId: 'V_Message_List',//API ID
    //   oParamValue: {
    //     "ticnum": ticnum
    //   }
    // }

    request.post({
        headers: {
            'Content-Type': 'text/plain;charset=UTF-8'

        },
        url: searchapi,
        form: JSON.stringify(body)
    }, function (error, response, body) {
        console.log('Get ticketid success!')
        // console.log(body)
        // let replys = JSON.parse(body).record
        // console.log(replys)
        // console.log(replys[1])
        res.render('message', {
            title: 'Detail',
            rows: JSON.parse(body).record,
        });
        //console.log(JSON.parse(body).record)
    });
});



/*POST from page */

// router.post('/', FormParser.single('upimg'), function (req, res, next) {
router.post('/', FormParser.array('upimg'), function (req, res) {

    // let upimg;
    let upimgs = req.files

    let fields = req.body

    //回覆訊息
    let msgInfo = {
        msgBody: {
            ticket_id: fields.ticket_id,
            pid: fields.pid,
            name: fields.name,
            mobile: fields.mobile,
            mcontent: fields.content,
            maincat: fields.mcate,
            subcat: fields.scate,
            dept: fields.agency,
            area: fields.area
            // imgname: null
        },
        // hasImg: false,
        imgs: []
    }

    if (upimgs.length != 0) {
        upimgs.forEach(upimg => {
            msgInfo.imgs.push({ filename: upimg.filename, path: upimg.path })
        })
    }

    // if (req.file === undefined) {
    //     msgInfo.hasImg = false
    //     msgInfo.msgBody.imgname = null
    // } else {
    //     upimg = req.file
    //     let imgName = `${upimg.filename}.${upimg.mimetype.split('/')[1]}`
    //     let imgPath = upimg.destination + imgName
    //     fs.renameSync(upimg.path, imgPath)
    //     msgInfo.hasImg = true
    //     msgInfo.msgBody.imgname = imgName
    //     msgInfo.imgs.push({
    //         filename: imgName,
    //         path: imgPath
    //     });

    // }

    let remsg = new msghandler.Message(msgInfo)
    remsg.seeVar();
    remsg.saveToS3();
    remsg.inserttoarcare();
    // res.redirect('back');
    // res.render('reportend');
    res.send(true)
});


module.exports = router;