const loop = require('./loop')

exports.send = function (agid_dept, msg, fileStreams) {
    let uid = "d3d2e3ba-aec7-40c7-8385-5376fa6975b8", // loop
        pid = "64575e59-6ff7-4ba8-8509-6d8861ab8202", // loop
        serverkey = "",
        accesstoken = "",
        mobile = "6312345678",
        salt = "",
        pwsalt = "",
        agid_robot = "794fe0b8-4dac-483f-9728-3663eb093d4d" // loop 

    this.agid_dept = agid_dept

    // console.log(msg);

    // 取得Salt
    loop.getSalt(1, function (err, Salt) {
        if (err) console.log(err)
        salt = Salt

        // 取得PwSalt
        loop.getPwSalt(1, "mobile", mobile, function (err, pwSalt) {
            if (err) console.log(err)
            pwsalt = pwSalt

            // 用ID 密碼取得Token
            loop.getToken(1, mobile, "wattertek123", pwSalt, salt, function (err, serverKey, Token, Uid) {
                if (err) console.log(err)
                accesstoken = Token
                serverkey = serverKey
                uid = Uid

                // // Follow Agent
                // loop.doFollowAgent(1, Uid, Token, pid, agid, (err, body) => {
                //     if (err) console.log(err)
                //     console.log(JSON.parse(body).rsp.value.rlt.type)
                // })

                // 發送訊息to 8888 robot
                loop.doSendMsg(1, Uid, accesstoken, pid, agid_robot, msg, (err, body, response) => {
                    if (err) console.log(err)
                    console.log("Send to Robot - message finished")
                })

                if (fileStreams.length != 0) {
                    fileStreams.forEach((stream, index) => {
                        loop.doSendImage(1, Uid, accesstoken, pid, agid_robot, stream, (err, body, response) => {
                            if (err) console.log(err)
                            console.log(`Send to Robot - image ${index + 1} finished`)
                        })
                    })
                }

                // // 發送訊息to agent
                // if (agid_dept !== '') {
                //     loop.doSendMsg(1, Uid, Token, pid, agid_dept, msg, (err, body, response) => {
                //         if (err) console.log(err)
                //         console.log("Send message to agent finished")
                //     })
                // }
            })
        })
    })
}