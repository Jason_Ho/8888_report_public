const express = require('express');
const path = require('path');

// 可設定網站icon
const favicon = require('serve-favicon');
// log檔
const logger = require('morgan');

const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const helmet = require('helmet')


const index = require('./routes/index');
const users = require('./routes/users');
const query = require('./routes/query');
const report_nofw = require('./routes/report_nofw');
const report_ofw = require('./routes/report_ofw')
const message = require('./routes/message');
const putmsg = require('./routes/putmsg');
const delphoto = require('./routes/delphoto');
const agreement = require('./routes/agreement');

const app = express();

// create a write stream (in append mode)
// var accessLogStream = fs.createWriteStream(__dirname + '/access.log', {flags: 'a'})

// setup the logger
// app.use(logger('combined', {stream: accessLogStream}))

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(helmet())

app.use(express.static(path.join(__dirname, 'public')));

app.use('/', index);
app.use('/users', users);
app.use('/query', query);
app.use('/report_nofw', report_nofw);
app.use('/report_ofw', report_ofw);
app.use('/message', message);
app.use('/putmsg',putmsg);
app.use('/delphoto',delphoto);
app.use('/agreement',agreement);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
